package com.spring.utils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;

@EnableFeignClients(basePackages = {"com.spring.utils", "com.spring.utils.clients"})
@ImportAutoConfiguration({FeignAutoConfiguration.class})
@SpringBootApplication
public class SpringUtilsApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringUtilsApplication.class, args);
    }
}