package com.spring.utils.utils;

import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.springframework.stereotype.Component;

import java.awt.*;

import static com.spring.utils.constants.UtilsConstants.FONT_WAS_NOT_FOUND_EXCEPTION;
import static com.spring.utils.constants.UtilsConstants.PATH_TO_CALISTOGA_REGULAR_FONT;

/**
 * @author Ibrahim Izirov
 * @since 08-08-2022
 */
@Component
public class PdfOptionsProvider {

    /**
     * Return new configuration with options and set font.
     *
     * @return PdfOptions as options with font
     */
    public static PdfOptions createPdfOptions() {
        PdfOptions options = PdfOptions.create();
        options.fontProvider((familyName, encoding, size, style, color)
                -> getFont(size, style, color));
        return options;
    }

    /**
     * Return new configuration with given path for font.
     *
     * @param size  Size of font
     * @param style - Style of font
     * @param color Color of font
     * @return new Font
     */
    private static Font getFont(float size, int style, Color color) {
        try {
            BaseFont baseFont = BaseFont.createFont(PATH_TO_CALISTOGA_REGULAR_FONT, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            return new Font(baseFont, size, style, color);
        } catch (Exception e) {
            throw new IllegalArgumentException(FONT_WAS_NOT_FOUND_EXCEPTION + e);
        }
    }
}