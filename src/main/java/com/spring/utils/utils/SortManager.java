package com.spring.utils.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class SortManager {

    /**
     * Time Complexity:
     * --------
     * Worst-case : 0(n²)- Since we loop through n elements n times, n being the length of the array,
     * * the time complexity of Bubble sort becomes 0(n²).
     * --------
     * Best-case : 0(n)- Since in this algorithm, we break our loop if our array is already sorted,
     * the best case time complexity will become O(n).
     * --------
     * Space Complexity: 0(1).
     * --------
     * Example: List<Integer> newlist = SortManager.bubbleSort (List, Integer::compare);
     */
    public static <T> void bubbleSort(List<T> list, Comparator<? super T> comparator) {
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = 0; j < list.size() - i - 1; j++) {
                if (comparator.compare(list.get(j), list.get(j + 1)) > 0) {
                    swap(list, j, j + 1);
                }
            }
        }
    }

    /**
     * Time Complexity:
     * --------
     * Worst-case: 0(n²) - Since for each element in the array, we traverse through
     * the remaining array to find the minimum, the time complexity will become 0(n²).
     * --------
     * Best-case: 0(n2)- Even if the array is already sorted, our algorithm looks for the minimum in
     * the rest of the array, and hence best-case time complexity is the same as worst-case.
     * --------
     * Space Complexity: 0(1).
     * --------
     * Example: List<Integer> newlist = SortManager.selectionSort(list, Integer::compare);
     */
    public static <T> void selectionSort(List<T> list, Comparator<? super T> comparator) {
        for (int i = 0; i < list.size() - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < list.size(); j++) {
                if (comparator.compare(list.get(j), list.get(minIndex)) < 0) {
                    minIndex = j;
                }
            }
            swap(list, i, minIndex);
        }
    }

    /**
     * Time Complexity:
     * --------
     * Worst-case: 0(n²) In worst case, our array is sorted in descending order. So for each element,
     * we have to keep traversing and swapping elements to the left.
     * --------
     * Best-case: 0(n)- In best case, our array is already sorted. So for each element, we compare our current
     * element to the element at the left only once. Since the order is correct, we don't swap and
     * move on to the next element. Hence, the time complexity will be O(n).
     * --------
     * Space Complexity: 0(1).
     * --------
     * Example: List<Integer> newList = SortManager.selectionSort(list, Integer::compare);
     */
    public static <T> void insertionSort(List<T> list, Comparator<? super T> comparator) {
        for (int i = 0; i < list.size(); i++) {
            int j = i;
            while (j > 0 && (comparator.compare(list.get(j), list.get(j - 1)) < 0)) {
                swap(list, j, j - 1);
                j--;
            }
        }
    }

    /**
     * Time Complexity:
     * --------
     * Worst-case: 0(n²)- When the array is sorted in descending order or all the elements are the same
     * in the array, the time complexity jumps to O(n) since the subarrays are highly unbalanced.
     * --------
     * Best-case: O(nlogn) First, we are dividing the array into two subarrays recursively which will cost
     * a time complexity of O(logn). For each function call, we are calling the partition function
     * which costs O(n) time complexity. Hence the total time complexity is O(nlogn).
     * --------
     * Space Complexity: 0(n)
     * --------
     * Example for ASC direction: SortManager.quickSort (list, Integer::compare);
     * Example for DESC direction: SortManager.quickSort (list, Comparator.reverseOrder());
     */
    public static <T> void quickSort(List<T> list, Comparator<? super T> comparator) {
        quickSort(list, 0, list.size() - 1, comparator);
    }

    /**
     * /**
     * Time Complexity:
     * --------
     * Worst-case: O(nlogn)- The worst-case time complexity is same as best case.
     * --------
     * Best-case: O(nlogn)- First, we are dividing the array into two subarrays recursively which will
     * cost a time complexity of 0(logn). For each function call, we are calling the partition function
     * which costs O(n) time complexity. Hence the total time complexity is O(nlogn).
     * --------
     * Space Complexity: 0(n)
     * --------
     * Example for ASC direction: SortManager.mergeSort(list, Integer::compare);
     * Example for DESC direction: SortManager.mergeSort (list, Comparator.reverseOrder());
     */
    public static <T> void mergeSort(List<T> list, Comparator<? super T> comparator) {
        int size = list.size();
        if (size < 2) {
            return;
        }
        int half = size / 2;
        List<T> left = new ArrayList<>(list.subList(0, half));
        List<T> right = new ArrayList<>(list.subList(half, size));

        mergeSort(left, comparator);
        mergeSort(right, comparator);
        merge(left, right, list, comparator);
    }


    /**
     * Particular quick sort method.
     */
    private static <T> void quickSort(List<T> list, int low, int high, Comparator<? super T> comparator) {
        if (low < high + 1) {
            int p = partitionQuickSort(list, low, high, comparator);
            quickSort(list, low, p - 1, comparator);
            quickSort(list, p + 1, high, comparator);
        }
    }

    /**
     * Get pivot fir quick sort.
     */
    private static int getPivot(int low, int high) {
        Random random = new Random();
        return random.nextInt((high - low) + 1) + low;
    }

    /**
     * Choice for partition on quick sort.
     */
    private static <T> int partitionQuickSort(List<T> list, int low, int high, Comparator<? super T> comparator) {
        swap(list, low, getPivot(low, high));
        int border = low + 1;
        for (int i = border; i <= high; i++) {
            if ((comparator.compare(list.get(i), list.get(low)) < 0)) {
                swap(list, i, border++);
            }
        }
        swap(list, low, border - 1);
        return border - 1;
    }

    /**
     * Merge used for merge sort algorithm.
     */
    private static <T> void merge(List<T> left, List<T> right, List<T> original, Comparator<? super T> comparator) {
        int leftIndex = 0;
        int rightIndex = 0;
        int originalIndex = 0;

        while (leftIndex < left.size() && rightIndex < right.size()) {
            if ((comparator.compare(left.get(leftIndex), right.get(rightIndex)) < 0)) {
                original.set(originalIndex, left.get(leftIndex));
                leftIndex++;
            } else {
                original.set(originalIndex, right.get(rightIndex));
                rightIndex++;
            }
            originalIndex++;
        }
        while (leftIndex < left.size()) {
            original.set(originalIndex, left.get(leftIndex));
            originalIndex++;
            leftIndex++;
        }
        while (rightIndex < right.size()) {
            original.set(originalIndex, right.get(rightIndex));
            originalIndex++;
            rightIndex++;
        }
    }

    /**
     * Method for swap elements in collection.
     */
    private static <T> void swap(List<T> list, int index1, int index2) {
        T temp = list.get(index1);
        list.set(index1, list.get(index2));
        list.set(index2, temp);
    }
}