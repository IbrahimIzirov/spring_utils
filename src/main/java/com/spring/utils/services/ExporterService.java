package com.spring.utils.services;

import com.spring.utils.constants.DocumentType;
import com.spring.utils.models.ModelWithBLOB;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Service that are creating rest calls to export pdf file.
 *
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
public interface ExporterService {

    /**
     * Method who export ModelWithBLOB in pdf file.
     *
     * @param response -HttpServletResponse
     * @param type     -DocumentType
     * @param blob     - ModelWithBLOB
     */
    void export(HttpServletResponse response, DocumentType type, ModelWithBLOB blob) throws IOException;

    /**
     * Set octet stream to response content type and add name for exported document
     *
     * @param response     - HttpServletResponse
     * @param documentType - DocumentType
     * @param blob         - ModelWithBLOB
     */
    void writeHeaderValue(HttpServletResponse response, DocumentType documentType, ModelWithBLOB blob);
}