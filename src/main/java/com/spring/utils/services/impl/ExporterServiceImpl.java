package com.spring.utils.services.impl;

import com.spring.utils.constants.DocumentType;
import com.spring.utils.constants.Profession;
import com.spring.utils.models.ModelWithBLOB;
import com.spring.utils.services.ExporterService;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.spring.utils.constants.UtilsConstants.*;
import static com.spring.utils.utils.PdfOptionsProvider.createPdfOptions;

/**
 * Implements methods that are creating rest calls to export word file.
 *
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
@Service
public class ExporterServiceImpl implements ExporterService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExporterServiceImpl.class);

    @Override
    public void export(HttpServletResponse response, DocumentType type, ModelWithBLOB blob) throws IOException {
        switch (type) {
            case DOCUMENT_WITH_IMAGE:
                exportDocumentWithImage(response, blob);
                break;
            case DOCUMENT_WITH_TABLE:
                exportDocumentWithTable(response, blob);
                break;
        }
    }

    @Override
    public void writeHeaderValue(HttpServletResponse response, DocumentType documentType, ModelWithBLOB blob) {
        response.setContentType(APPLICATION_OCTET_STREAM);
        String headerValue = String.format("attachment; filename=%s_%s.pdf",
                documentType, "EXAMPLE_NAME");
        response.setHeader(CONTENT_DISPOSITION_HEADER_KEY, headerValue);
    }

    /**
     * Method who write form with image
     *
     * @param response -HttpServletResponse
     * @param blob     - ModelWithBLOB
     */
    private void exportDocumentWithImage(HttpServletResponse response, ModelWithBLOB blob) throws IOException {
        Path templatePath = Paths.get(ABSOLUTE_PATH_DOCUMENT_WITH_IMAGE);
        try (XWPFDocument doc = new XWPFDocument(Files.newInputStream(templatePath))) {
            writeHeaderSectionDocWithImage(blob, doc);
            ServletOutputStream outputStream = response.getOutputStream();

            // Used for generating .docx documents.
//            doc.write(outputStream);

            PdfConverter.getInstance().convert(doc, outputStream, createPdfOptions());
        }
        LOGGER.info("New document with image was exported.");
    }

    /**
     * Method who write form with table
     *
     * @param response -HttpServletResponse
     * @param blob     - ModelWithBLOB
     */
    private void exportDocumentWithTable(HttpServletResponse response, ModelWithBLOB blob) throws IOException {
        Path templatePath = Paths.get(ABSOLUTE_PATH_DOCUMENT_WITH_TABLE);
        try (XWPFDocument doc = new XWPFDocument(Files.newInputStream(templatePath))) {
            writeHeaderSectionDocWithTable(blob, doc);
            writeCheckBoxesValues(blob, doc);
            ServletOutputStream outputStream = response.getOutputStream();

            // Used for generating .docx documents.
//            doc.write(outputStream);

            PdfConverter.getInstance().convert(doc, outputStream, createPdfOptions());
        }
        LOGGER.info("New document with table was exported.");

    }

    /**
     * Method who write header section in document with image
     *
     * @param blob - ModelWithBLOB
     * @param doc  - XWPFDocument
     */
    private void writeHeaderSectionDocWithImage(ModelWithBLOB blob, XWPFDocument doc) {
        replaceTextInDoc(doc, "textForHeader", "Текс с който ще заменя думата в хедъра");
        replaceTextInDoc(doc, "threeText ", "Текс под снимката 1");
        replaceTextInDoc(doc, "optionTwo ", "Текс под снимката 2");
    }

    /**
     * Method who write header section in document with table
     *
     * @param blob - ModelWithBLOB
     * @param doc  - XWPFDocument
     */
    private void writeHeaderSectionDocWithTable(ModelWithBLOB blob, XWPFDocument doc) {
        replaceTextInDoc(doc, "textForHeader", "Текс с който ще заменя думата в хедъра");
        replaceTextInDoc(doc, "questOne ", "Текс 1");
        replaceTextInDoc(doc, "twoQu ", "Текс 2");
        replaceTextInDoc(doc, "threeInf ", "Текс 3");
        replaceTextInDoc(doc, "Inforam ", "Текс 4");
    }

    /**
     * Method who replace profession in checkboxes
     *
     * @param blob - ModelWithBLOB
     * @param doc  - XWPFDocument
     */
    private void writeCheckBoxesValues(ModelWithBLOB blob, XWPFDocument doc) {
        List<String> professionWordInDocument = new ArrayList<>(List.of("infoOne", "toInf", "kowSj", "lowsa"));

        for (int i = 0; i < Profession.values().length; i++) {
            Profession currentProfession = Profession.values()[i];
            if (blob.getProfession().equalsIgnoreCase(currentProfession.displayName())) {
                String checkBoxTrue = String.format(STRING_WITH_CHECKBOX, currentProfession.displayName());
                replaceTextInTable(doc, professionWordInDocument.get(i), checkBoxTrue);
            } else {
                String checkBoxFalse = String.format(STRING_WITHOUT_CHECKBOX, currentProfession.displayName());
                replaceTextInTable(doc, professionWordInDocument.get(i), checkBoxFalse);
            }
        }
    }

    /**
     * Method who replace text in word document(table)
     *
     * @param doc         - XWPFDocument document
     * @param findText    - find word to replace
     * @param replaceText - word for replace text
     */
    private void replaceTextInTable(XWPFDocument doc, String findText, String replaceText) {
        doc.getTables().forEach(table -> {
            table.getRows().forEach(row -> {
                row.getTableCells().forEach(cell -> {
                    cell.getParagraphs().forEach(paragraph -> {
                        paragraph.getRuns().forEach(run -> {
                            String text = run.text();
                            if (text.contains(findText)) {
                                if (replaceText.contains("\n")) {
                                    String[] lines = replaceText.split("\n");
                                    run.setText(lines[0], 0);
                                    for (int i = 1; i < lines.length; i++) {
                                        run.addBreak();
                                        run.setText(lines[i]);
                                    }
                                } else {
                                    run.setText(text.replace(findText, replaceText), 0);
                                }
                            }
                        });
                    });
                });
            });
        });
    }

    /**
     * Method who replace text in word document(not in table)
     *
     * @param doc         - XWPFDocument document
     * @param findText    - find word to replace
     * @param replaceText - word for replace text
     */
    private void replaceTextInDoc(XWPFDocument doc, String findText, String replaceText) {
        doc.getParagraphs().forEach(p -> {
            p.getRuns().forEach(run -> {
                String text = run.text();
                if (text.contains(findText)) {
                    run.setText(text.replace(findText, replaceText), 0);
                }
            });
        });
    }
}