package com.spring.utils.services.impl;

import com.spring.utils.exceptions.EntityNotFoundException;
import com.spring.utils.models.ModelWIthBLOBPaginated;
import com.spring.utils.models.ModelWithBLOB;
import com.spring.utils.models.PaginationResponse;
import com.spring.utils.models.dto.PaginationRequest;
import com.spring.utils.repositories.ModelRepository;
import com.spring.utils.repositories.PaginatedWithJPA;
import com.spring.utils.services.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import static com.spring.utils.constants.UtilsConstants.ENTITY_WAS_NOT_FOUND;

/**
 * Implements methods that are creating rest calls to manege with models.
 *
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;
    private final PaginatedWithJPA paginatedWithJPA;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository,
                            PaginatedWithJPA paginatedWithJPA) {
        this.modelRepository = modelRepository;
        this.paginatedWithJPA = paginatedWithJPA;
    }

    @Override
    public ModelWithBLOB getModelWithBLOBById(long id) {
        ModelWithBLOB model = modelRepository.getModelWithBLOBById(id);
        if (model == null) {
            throw new EntityNotFoundException(String.format(ENTITY_WAS_NOT_FOUND, id));
        }
        return model;
    }

    @Override
    public PaginationResponse getAllModels(PaginationRequest paginationRequest) {
        PaginationResponse response = new PaginationResponse();
        List<ModelWIthBLOBPaginated> models = paginatedWithJPA.findAllModels(
                String.valueOf(Timestamp.valueOf(paginationRequest.getStartDate())),
                String.valueOf(Timestamp.valueOf(paginationRequest.getEndDate())),
                paginationRequest.getFilterData().getFirstName() != null ?
                        paginationRequest.getFilterData().getFirstName().toLowerCase(Locale.ROOT) : null,
                paginationRequest.getFilterData().getLastName() != null ?
                        paginationRequest.getFilterData().getLastName().toLowerCase(Locale.ROOT) : null,
                paginationRequest.getFilterData().getId(),
                paginationRequest.getFilterData().getCategory(),
                paginationRequest.getOrderData().getId(),
                paginationRequest.getOrderData().getFirstName(),
                paginationRequest.getOrderData().getLastName(),
                paginationRequest.getOrderData().getCategory(),
                paginationRequest.getDesiredPage(),
                paginationRequest.getMaxRows()
        );
        response.setModels(models);
        if (!models.isEmpty()) {
            response.setTotalRecords(models.get(0).getTotalSize());
        }
        return response;
    }
}