package com.spring.utils.services;

import com.spring.utils.models.ModelWithBLOB;
import com.spring.utils.models.PaginationResponse;
import com.spring.utils.models.dto.PaginationRequest;

/**
 * Service that are creating rest calls to operate with models.
 *
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
public interface ModelService {

    /**
     * Method who return ModelWithBLOB by given id.
     *
     * @param id -if of model
     * @return ModelWithBLOB
     */
    ModelWithBLOB getModelWithBLOBById(long id);

    /**
     * Method who return List of models paginated by given filter and order criteria.
     *
     * @param paginationRequest - contains start, end date and filter and order objects.
     * @return PaginationResponse
     */
    PaginationResponse getAllModels(PaginationRequest paginationRequest);
}