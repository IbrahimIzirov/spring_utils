package com.spring.utils.services.soap;

public interface MessageService {

    /**
     * Command used fo sending messages.
     */
    String sendMessage(String parameters);
}
