package com.spring.utils.services.soap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.bind.JAXBElement;


public class MessageServiceImpl extends WebServiceGatewaySupport implements MessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Value("${soap.url.example.service}")
    private String exampleServiceUrl;

    @Value("${soap.action.example.service.send.message}")
    private String exampleServiceSendSMSAction;

    @Override
    public String sendMessage(String parameters) {
        String message = "";
        JAXBElement<String> jaxbElementRequest = null;
        JAXBElement<String> jaxbElementResponse = (JAXBElement<String>) getWebServiceTemplate().marshalSendAndReceive(
                exampleServiceUrl, jaxbElementRequest, new SoapActionCallback(exampleServiceSendSMSAction));

        return jaxbElementResponse.getValue();
    }
}