package com.spring.utils.services.rest;

import com.spring.utils.clients.CustomerServiceFeignClient;
import com.spring.utils.models.dto.ObjectBlobDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerFeignClientService {

    private final CustomerServiceFeignClient feignClient;

    @Autowired
    public CustomerFeignClientService(CustomerServiceFeignClient feignClient) {
        this.feignClient = feignClient;
    }

    public ObjectBlobDto getCustomerInfoById(long id) {
        return feignClient.getCustomerById(id);
    }
}
