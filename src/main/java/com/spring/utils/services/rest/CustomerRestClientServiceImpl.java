package com.spring.utils.services.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.utils.exceptions.RestTemplateErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.spring.utils.constants.UtilsConstants.HEADER_COOKIE;
import static com.spring.utils.constants.UtilsConstants.HEADER_COOKIE_VALUE;

/**
 *
 */
@Service
public class CustomerRestClientServiceImpl {

    @Value("${rest.url.customer-info}")
    private String customerInfoRestUrl;

    private final RestTemplate restTemplate;

    @Autowired
    public CustomerRestClientServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getCustomerInfoById(long id) {
        ObjectMapper mapper = new ObjectMapper();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HEADER_COOKIE, HEADER_COOKIE_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        this.restTemplate.setErrorHandler(new RestTemplateErrorHandler());
        ResponseEntity<String> response = this.restTemplate.exchange(
                customerInfoRestUrl + id + "?source=POW24", HttpMethod.GET, entity, String.class);

        String responseBody = response.getBody();

        // Here we need to use mapper for mapped object from string to some entity
        // ResponseObjectModel mappedObject = mapper.readValue(responseBody, ResponseObjectModel.class);


        // After that return that object, here for example is String
        return "";
    }
}