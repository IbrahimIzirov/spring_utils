package com.spring.utils.repositories;

import com.spring.utils.models.Application;
import com.spring.utils.models.ReportProducts;
import com.spring.utils.models.dto.ReportRequest;

import java.util.List;

/**
 * Reports repository with custom queries.
 *
 * @author Ibrahim Izirov
 * @since 10-05-2023
 */
public interface CustomReportsRepository {

    /**
     * Return list of reports for globally application.
     */
    List<Application> createMainReport(ReportRequest reportRequest);

    /**
     * Return list of reports which contains counts of every single one of them.
     */
    List<ReportProducts> createReportForProducts(ReportRequest reportRequest);

}