package com.spring.utils.repositories;

import com.spring.utils.models.ModelWithBLOB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository with operations to access object from database, define and execute queries, and more.
 *
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
@Repository
public interface ModelRepository extends JpaRepository<ModelWithBLOB, Long> {

    /**
     * Find model with given id
     *
     * @param id - id of model
     * @return ModelWithBLOB
     */
    @Query(value = "select * from SCHEMA.TABLE_NAME p where p.ID = ?1 ",
            nativeQuery = true)
    ModelWithBLOB getModelWithBLOBById(long id);
}