package com.spring.utils.repositories;

import com.spring.utils.models.ModelWIthBLOBPaginated;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Paginated repository via JPA
 *
 * @author Ibrahim Izirov
 * @since 05-04-2022
 */
@Repository
public interface PaginatedWithJPA extends JpaRepository<ModelWIthBLOBPaginated, Long> {


    /**
     * return List of models by given OrderData and FilterData and total size paginated by SQL.
     * If you want to work this query, you should add in ENTITY Class --> new Column
     * * @Column(name = "TOTAL_SIZE")
     * * private Long totalSize
     *
     * @return List of applications
     */

    @Query(value = "select p.*, COUNT(*) OVER() As TOTAL_SIZE from TABLE_NAME.MODEL p " +
            "LEFT OUTER JOIN TABLE_NAME.STATUSES s ON p.STATUS = s.STATUS " +
            "where to char(p.DATE_LOADED, 'YYYY-MM-DD HH24:MI:SS.FF') >= ?1 and " +
            "to_char(p.DATE_LOADED, 'YYYY-MM-DD HH24: MI:SS.FF') <= ?2 and p.category_1 = 'Test' and " +
            "s.STATUS not in 'TEST_STATUS' and " +
            "(?3 is null OR lower(json_value (p.properties, '$. firstName')) = ?3) and " +
            "(?4 is null OR lower(json_value(p.properties, '$.lastName')) = ?4) and " +
            "(?5 is null OR to char(p.object_id) = ?5) and " +
            "(?6 is null OR p.category_3 = ?6) and " +
            "ORDER BY " +
            "CASE WHEN ?7 = 'DESC' THEN p.object_id END DESC, " +
            "CASE WHEN ?7 = 'ASC' THEN p.object_id END ASC, " +
            "CASE WHEN ?8 = 'DESC' THEN json_value(p.properties, '$.firstName') END DESC, " +
            "CASE WHEN ?8 = 'ASC' THEN json_value(p.properties, '$.firstName') END ASC, " +
            "CASE WHEN ?9 = 'DESC' THEN json_value(p.properties, '$.lastName') END DESC, " +
            "CASE WHEN ?9 = 'ASC' THEN json_value(p.properties, '$.lastName') END ASC, " +
            "CASE WHEN ?10 = 'DESC' THEN p.category_3 END DESC, " +
            "CASE WHEN ?10 = 'ASC' THEN p.category_3 END ASC OFFSET nvl(?11-1,1)*?12 ROWS FETCH NEXT ?12 ROWS ONLY ",
            nativeQuery = true)
    List<ModelWIthBLOBPaginated> findAllModels(String startDate, String endDate, String firstNameFilter,
                                                     String lastNameFilter, String idFilter, String categoryThreeFilter,
                                                     String idOrder, String firstNameOrder, String lastNameOrder,
                                                     String categoryThreeOrder, int pageNo, int maxRows);

}