package com.spring.utils.repositories;

import com.spring.utils.models.ApplicationPagination;
import com.spring.utils.models.dto.PaginationRequest;

import java.util.List;

/**
 * Paginated repository with custom queries.
 *
 * @author Ibrahim Izirov
 * @since 04-05-2023
 */
public interface CustomPaginatedRepository {

    /**
     * Find all applications by given criteria of properties in FilterData and OrderData.
     *
     * @param request - Request with all nedeed information for data.
     * @return list of applications
     */
    List<ApplicationPagination> findAllApplications(PaginationRequest request);
}