package com.spring.utils.repositories.impl;

import com.spring.utils.models.Application;
import com.spring.utils.models.ReportOneProductOnly;
import com.spring.utils.models.ReportProducts;
import com.spring.utils.models.dto.ReportRequest;
import com.spring.utils.repositories.CustomReportsRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.List;

/**
 * Reports repository implementation.
 *
 * @author Ibrahim Izirov
 * @since 10-05-2023
 */
public class CustomReportsRepositoryImpl implements CustomReportsRepository {

    private final EntityManager entityManager;

    @Autowired
    public CustomReportsRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Application> createMainReport(ReportRequest reportRequest) {
        Query query = entityManager.createNativeQuery(createCustomMainReport(reportRequest), Application.class);
        return query.getResultList();
    }

    @Override
    public List<ReportProducts> createReportForProducts(ReportRequest reportRequest) {
        // Here no mather if the response of method is ReportProduct or String or something else,
        // This method will produce ReportProducts or ReportOneProductOnly.
        Query query = entityManager.createNativeQuery(createCustomProductsReport(reportRequest),
                reportRequest.getProduct() == null ? ReportProducts.class : ReportOneProductOnly.class);
        return query.getResultList();
    }

    private String createCustomMainReport(ReportRequest reportRequest) {
        return String.format("select a.* from SCHEMA.TABLE_NAME a " +
                        "LEFT OUTER JOIN SHEMA.TABLE_NAME_FOR_JOIN s ON a.STATUS = s.STATUS " +
                        "WHERE to_char(p.DATE_LOADED, 'YYYY-MM-DD HH24:MI:SS.FF')>= '%s' " +
                        "and to_char(p.DATE_LOADED, 'YYYY-MM-DD HH24:MI:SS.FF') <= '%s' and p.INSTANCE = 'POW52' " +
                        "ORDER BY a.id DESC",
                Timestamp.valueOf(reportRequest.getStartDate()),
                Timestamp.valueOf(reportRequest.getEndDate()));
    }

    private String createCustomProductsReport(ReportRequest reportRequest) {
        StringBuilder builder = new StringBuilder();

        // Append select clause at the beginning with status type
        builder.append(reportRequest.getStatus().equalsIgnoreCase("status") ?
                "SELECT NVL(s.STATUS, 'N/A) " : "SELECT NVL(json_value(a.json_table, '$.source'), 'N/A') ");

        String mainQuery = String.format("AS STATUS, %s " +
                        "from SCHEMA.TABLE_NAME a LEFT OUTER JOIN SHEMA.TABLE_NAME_FOR_JOIN s ON a.STATUS = s.STATUS " +
                        "WHERE to_char(p.DATE_LOADED, 'YYYY-MM-DD HH24:MI:SS.FF')>= '%s' " +
                        "and to_char(p.DATE_LOADED, 'YYYY-MM-DD HH24:MI:SS.FF') <= '%s' and p.INSTANCE = 'POW52'",
                reportRequest.getProduct() == null ? createCountOperation() : "count(*) as COUNTS",
                Timestamp.valueOf(reportRequest.getStartDate()),
                Timestamp.valueOf(reportRequest.getEndDate()));

        builder.append(mainQuery);

        // Append group by clause at the end of query with given type.
        builder.append(reportRequest.getStatus().equalsIgnoreCase("status") ?
                "GROUP BY s.STATUS" : " GROUP BY json_value(a.json_table, '$.source')");

        return builder.toString();
    }

    private String createCountOperation() {
        return "count(CASE WHEN a.PRODUCT_CODE = 'LG' THEN 1 END) AS LG_COUNTS, " +
                "count(CASE WHEN a.PRODUCT_CODE = 'SONY' THEN 1 END) AS SONY_COUNTS, " +
                "count(CASE WHEN a.PRODUCT_CODE = 'NOKIA' THEN 1 END) AS NOKIA_COUNTS, " +
                "count(CASE WHEN a.PRODUCT_CODE = 'SAMSUNG' THEN 1 END) AS SAMSUNG_COUNTS, " +
                "count(CASE WHEN a.PRODUCT_CODE = 'TOSHIBA' THEN 1 END) AS TOSHIBA_COUNTS, " +
                "count(CASE WHEN a.PRODUCT_CODE = 'DELL' THEN 1 END) AS DELL_COUNTS";
    }
}