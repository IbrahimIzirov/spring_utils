package com.spring.utils.repositories.impl;

import com.spring.utils.models.ApplicationPagination;
import com.spring.utils.models.dto.PaginationRequest;
import com.spring.utils.repositories.CustomPaginatedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.spring.utils.constants.UtilsConstants.*;

/**
 * Custom repository for applications implements all operations to access data.
 */
@Repository
public class PaginatedRepositoryImpl implements CustomPaginatedRepository {

    @Value("${environment}")
    private String environment;

    private final EntityManager entityManager;

    @Autowired
    public PaginatedRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<ApplicationPagination> findAllApplications(PaginationRequest request) {
        Query query = entityManager.createNativeQuery(createCustomQuery(request), ApplicationPagination.class);
        return query.getResultList();
    }


    private String createCustomQuery(PaginationRequest request) {
        StringBuilder builder = new StringBuilder();

        //Here start the query.
        String firstQuery = String.format("select %s p.*, %s from DATABASE.TABLE_NAME p, " +
                        "json_table(p.json_column,'$' columns (" +
                        "first_name path '$.firstName', " +
                        "last_name path '$.lastName', " +
                        "eik path '$.eik', " +
                        "customer_is_legal path '$.SearchForCustomer.customerIsLegal')) as json " +
                        "WHERE to_char(p.DATE_LOADED, 'YYYY-MM-DD HH24:MI:SS.FF')>= '%s' " +
                        "and to_char(p.DATE_LOADED, 'YYYY-MM-DD HH24:MI:SS.FF') <= '%s' and p.INSTANCE = 'POW52' " +
                        "and p.status not in ('TEMP', 'FORBIDDEN')",
                Stream.of(request.getFilterData().getFirstName(),
                        request.getFilterData().getLastName(),
                        request.getFilterData().getCategory())
                        .allMatch(Objects::isNull)
                        ? environment.equalsIgnoreCase("PROD") ? INDEXES_NAMES_PROD : INDEXES_NAMES_TEST
                        : "/*+INDEX(p, BLOB_COLUMN_INDEX_NAME)*/",
                String.format("%s' As TOTAL_SIZE", countNumberOfApplications(request)),
                Timestamp.valueOf(request.getStartDate()),
                Timestamp.valueOf(request.getEndDate()));

        builder.append(firstQuery);

        // Create filter clause for query
        createFilterClause(request, builder);

        // Create order clause for query
        createOrderClause(request, builder);

        // Append pagination at the end of query with following command.
        builder.append(String.format(" OFFSET nvl(%d-1,1)*%d ROWS FETCH NEXT %d ROWS ONLY",
                request.getDesiredPage(),
                request.getMaxRows(),
                request.getMaxRows()));

        return builder.toString();
    }

    /**
     * Count numnber of application executable in main query
     *
     * @param request - Pagination request
     * @return count of applications as number
     */
    private BigDecimal countNumberOfApplications(PaginationRequest request) {
        StringBuilder builder = new StringBuilder();
        List<String> statuses = request.getFilterData().getStatuses();

        String countQuery = String.format("SELECT count(*) AS TOTAL_SIZE from DATABASE.TABLE_NAME p " +
                        "WHERE to_char(p.DATE_LOADED, 'YYYY-MM-DD HH24:MI:SS.FF')>= '%s' " +
                        "and to_char(p.DATE_LOADED, 'YYYY-MM-DD HH24:MI:SS.FF') <= '%s' and p.INSTANCE = 'POW52' " +
                        "%s",
                Timestamp.valueOf(request.getStartDate()),
                Timestamp.valueOf(request.getEndDate()),
                statuses != null && !statuses.isEmpty() ? "" : STATUSES_FOR_COUNTING_OPERATION);

        builder.append(countQuery);

        // Create filter clause for counting query
        createFilterClause(request, builder);

        Query query = entityManager.createNativeQuery(builder.toString());
        return (BigDecimal) query.getSingleResult();
    }

    /**
     * Custom filter clause for paginated operation.
     */
    private void createFilterClause(PaginationRequest request, StringBuilder builder) {

        filterChecker(request.getFilterData().getFirstName(),
                " AND CONTAINS (p.json_column, lower('firstName:%s')) > 0", builder, true);
        filterChecker(request.getFilterData().getLastName(),
                " AND CONTAINS (p.json_column, lower('lastName:%s')) > 0", builder, true);
        filterChecker(request.getFilterData().getId(),
                " and to_char(p.id)= '%s'", builder, false);
        filterChecker(request.getFilterData().getCategory(), " and lower(p.CATEGORY) = '%s'", builder, true);

        List<String> statuses = request.getFilterData().getStatuses();
        if (statuses != null && !statuses.isEmpty()) {
            String queryStatuses = statuses.stream()
                    .map(status -> String.format("'%s'", status))
                    .collect(Collectors.joining("", "(", ")"))
                    .replace("TRIGGER_IRAST", "'OOS_2', 'OOS_3', 'OOS_4'")
                    .replace("''", "', '").toUpperCase(Locale.ROOT);

            filterChecker(queryStatuses, " and p.status IN %s", builder, false);
        }
    }

    private void createOrderClause(PaginationRequest request, StringBuilder builder) {
        orderChecker(request.getOrderData().getId(), "p.id", builder);
        orderChecker(request.getOrderData().getFirstName(), "first_name", builder);
        orderChecker(request.getOrderData().getLastName(), "last_name", builder);
        orderChecker(request.getOrderData().getCategory(), "p.PRODUCT", builder);
    }

    /**
     * Filter checker logic for main query
     */
    private void filterChecker(String filterElement, String tableElement, StringBuilder builder, boolean isLowerCase) {
        if (filterElement != null) {
            builder.append(String.format(tableElement,
                    isLowerCase ? filterElement.toLowerCase(Locale.ROOT) : filterElement
            ));
        }
    }

    /**
     * Order checker logic for main query
     */
    private void orderChecker(String orderElement, String tableElement, StringBuilder builder) {
        if (orderElement != null) {
            builder.append(" ORDER BY");
            if (orderElement.equalsIgnoreCase("DESC")) {
                builder.append(String.format("%s" + " DESC", tableElement));
            } else {
                builder.append(String.format("%s" + " ASC", tableElement));
            }
        }
    }
}