package com.spring.utils.config;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Authorization class for authorization with server on Key Cloak and JWT.
 *
 * @author Ibrahim Izirov
 * @since 01-04-2022
 */
@EnableWebSecurity
public class AuthorizationServerSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        //Uncomment this for TEST & PROD
        httpSecurity.authorizeRequests()
                .antMatchers("/v1/models/**")
                .access("hasAuthority('SCOPE_accessToUtilsCore') and hasAnyRole('model_viewer', 'admin')")
                .antMatchers("/v1/report/**")
                .access("hasAuthority('SCOPE_accessToUtilsCore') and hasAnyRole('report_viewer', 'admin')")
                .antMatchers("/actuator/health").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/v1/*").permitAll()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors()
                .and()
                .csrf()
                .disable()
                .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(jwtAuthenticationConverter("spring-util"));

//        Uncomment this for local(DEV) env
//        httpSecurity.authorizeRequests()
//                .antMatchers("/v3/api-docs/**").permitAll()
//                .antMatchers("/actuator/health").permitAll()
//                .antMatchers("/swagger-ui/**").permitAll()
//                .antMatchers("/v1/**").permitAll().anyRequest().authenticated()
//                .and().csrf().disable();
    }

    /**
     * Create custom converter to access roles for given client in key cloak instance, without this method in console
     * 'Grand Authorities' tab we can't view the ROLE_ prefix -> only SCOPE_ prefix.
     *
     * @param clientId - Id of client in key cloak.
     * @return JwtAuthenticationConverter -  Custom converter for access to roles in Key Cloak instance.
     */
    private JwtAuthenticationConverter jwtAuthenticationConverter(String clientId) {
        return new JwtAuthenticationConverter() {
            @Override
            protected Collection<GrantedAuthority> extractAuthorities(final Jwt jwt) {
                Collection<GrantedAuthority> authorities = super.extractAuthorities(jwt);
                Map<String, Object> resourceAccess = jwt.getClaim("resource_access");
                Map<String, Object> resource;
                Collection<String> resourceRoles;
                if (resourceAccess != null &&
                        (resource = (Map<String, Object>) resourceAccess.get(clientId)) !=
                                null && (resourceRoles = (Collection<String>) resource.get("roles")) != null)
                    authorities.addAll(resourceRoles.stream()
                            .map(x -> new SimpleGrantedAuthority("ROLE_" + x))
                            .collect(Collectors.toSet()));
                return authorities;
            }
        };
    }
}