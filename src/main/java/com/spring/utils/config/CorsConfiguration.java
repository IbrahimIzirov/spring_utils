package com.spring.utils.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Cors policy configuration, that is allowing origin for given environment, allowing all type of methods(such as
 * post, get, patch, delete etc.). Also this config is allowing all types of paths for this origin.
 *
 * @author Ibrahim Izirov
 * @since 01-04-2022
 */
@Configuration
public class CorsConfiguration implements WebMvcConfigurer {

    @Value("${cors-policy.allowed-origin}")
    private String allowedOrigin;

    @Value("${cors-policy.allowed-origin-url}")
    private String allowedOriginUrl;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins(allowedOrigin.equalsIgnoreCase("test") ? "*" : allowedOriginUrl)
                .allowedMethods("*");
    }
}