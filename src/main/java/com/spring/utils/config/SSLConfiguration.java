package com.spring.utils.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Class that configures ssl verification. Check all certificates and use them for connection client-server.
 *
 * @author Ibrahim Izirov
 * @since 26-04-2021
 */
@Configuration
public class SSLConfiguration {

    @Value("$(cacerts.path}")
    private String cacertsPath;

    @Value("${cacerts.password}")
    private String cacertsPassword;

    @Value("${url.address}")
    private String urlIpAddress;

    @Bean
    public void configureTrustStore() throws NoSuchAlgorithmException, KeyManagementException,
            KeyStoreException, CertificateException, IOException {

        X509TrustManager jvmTrustManager = getJVMTrustManager();
        X509TrustManager myTrustManager = getMyTrustManager();

        X509TrustManager mergedTrustManager = createMergedTrustManager(jvmTrustManager, myTrustManager);
        setSystemTrustManager(mergedTrustManager);

        // When we want to verify some IP address without having a certificate for it.
        HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> hostname.equals(urlIpAddress));
    }

    /**
     * Method that takes all JVM or JVH certificates
     *
     * @return X509TrustManager -  list of jvm certificates.
     */
    private X509TrustManager getJVMTrustManager() throws NoSuchAlgorithmException, KeyStoreException {
        return findDefaultTrustManager(null);
    }

    /**
     * Method that takes all local certificates based on keystore file.
     *
     * @return X509TrustManager - list of local certificates.
     */
    private X509TrustManager getMyTrustManager() throws KeyStoreException, IOException,
            NoSuchAlgorithmException, CertificateException {

        // Adapt to load your keystore
        try (FileInputStream myKeys = new FileInputStream(cacertsPath)) {
            KeyStore myTrustStore = KeyStore.getInstance("jks");
            myTrustStore.load(myKeys, cacertsPassword.toCharArray());

            return findDefaultTrustManager(myTrustStore);
        }
    }

    /**
     * Returns a new instance of the default TrustManager for this JVM. Uses the default JVM trust store when keystore file
     * is null, other hand when keystore is not null, then recognize all certificates from given keystore file.
     *
     * @param keyStore - Key store file
     * @return X509 TrustManager - list of certificates.
     */
    private X509TrustManager findDefaultTrustManager(KeyStore keyStore)
            throws NoSuchAlgorithmException, KeyStoreException {
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(keyStore); // If keyStore is null, tmf will be initialized with the default trust store

        for (TrustManager tm : tmf.getTrustManagers()) {
            if (tm instanceof X509TrustManager) {
                return (X509TrustManager) tm;
            }
        }
        return null;
    }

    /**
     * Get all certificates from jvm and local keystore file and merge then.
     *
     * @param jvmTrustManager    - JVN trust manager
     * @param customTrustManager - local trust manager
     * @return X509 TrustManager - list of merged certificates.
     */
    private X509TrustManager createMergedTrustManager(X509TrustManager jvmTrustManager,
                                                      X509TrustManager customTrustManager) {
        return new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                // If you're planning to use client-cert auth,
                // merge results from "defaultTrustManager" and "myTrustManager".
                return jvmTrustManager.getAcceptedIssuers();
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                try {
                    customTrustManager.checkServerTrusted(certs, authType);
                } catch (CertificateException e) {
                    // This will throw another Certificate Exception if this fails too.
                    jvmTrustManager.checkServerTrusted(certs, authType);
                }
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                // If you're planning to use client-cert auth,
                // do the same as checking the server.
                jvmTrustManager.checkClientTrusted(certs, authType);
            }
        };
    }

    /**
     * Method that set system trust manager.
     *
     * @param mergedTrustManager - List that contains all certificates.
     */
    private void setSystemTrustManager(X509TrustManager mergedTrustManager)
            throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, new TrustManager[]{mergedTrustManager}, null);

        // You don't have to set this as the default context,
        // it depends on the library you're using.
        SSLContext.setDefault(sslContext);
    }
}