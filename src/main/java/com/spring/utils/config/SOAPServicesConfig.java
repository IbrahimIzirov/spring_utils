package com.spring.utils.config;

import com.spring.utils.services.soap.MessageServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * Class that is configuring soap services.
 *
 * @author Ibrahim Izirov
 * @since 05-04-2023
 */
@Configuration
public class SOAPServicesConfig {

    @Value("${soap.default-url.example.service}")
    private String exampleServiceDefaultUrl;

    public SOAPServicesConfig() {
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in build.gradle
        marshaller.setContextPaths("message.provider.service", "msg.provider.service");
        return marshaller;
    }

    @Bean
    public MessageServiceImpl messageService(Jaxb2Marshaller marshaller) {
        MessageServiceImpl client = new MessageServiceImpl();
        client.setDefaultUri(exampleServiceDefaultUrl);
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);

        return client;
    }
}