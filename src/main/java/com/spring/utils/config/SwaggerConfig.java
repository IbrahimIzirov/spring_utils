package com.spring.utils.config;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.OAuthFlow;
import io.swagger.v3.oas.annotations.security.OAuthFlows;
import io.swagger.v3.oas.annotations.security.OAuthScope;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Swagger configuration class.
 *
 * @author Ibrahim Izirov
 * @since 01-04-2022
 */
@Configuration
@SecurityScheme(name = "spring-utils-security", type = SecuritySchemeType.OAUTH2,
        flows = @OAuthFlows(clientCredentials = @OAuthFlow(
                authorizationUrl = "${swagger-authorization-url}",
                tokenUrl = "${swagger-token-url}",
                scopes = {@OAuthScope(name = "openid", description = "openid")})))
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI().info(publicApi());
    }

    private Info publicApi() {
        return new Info()
                .title("Spring Utils service")
                .description("Microservice for manage all functions.")
                .version("1.0.0");
    }
}