package com.spring.utils.controllers;

import com.spring.utils.constants.DocumentType;
import com.spring.utils.models.ModelWithBLOB;
import com.spring.utils.models.PaginationResponse;
import com.spring.utils.models.dto.PaginationRequest;
import com.spring.utils.services.ExporterService;
import com.spring.utils.services.ModelService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.concurrent.Callable;

import static com.spring.utils.constants.SwaggerConstants.*;

/**
 * Controller with operations for Utils.
 *
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
@ApiResponses(value = {
        @ApiResponse(code = 200, message = RESPONSE_STATUS_OK),
        @ApiResponse(code = 400, message = RESPONSE_STATUS_BAD_REQUEST),
        @ApiResponse(code = 401, message = RESPONSE_STATUS_UNAUTHORIZED),
        @ApiResponse(code = 403, message = RESPONSE_STATUS_FORBIDDEN),
        @ApiResponse(code = 404, message = RESPONSE_STATUS_NOT_FOUND),
        @ApiResponse(code = 500, message = RESPONSE_STATUS_INTERNAL_SERVER_ERROR)
})
@SecurityRequirement(name = "spring-utils-security")
@RestController
@RequestMapping("/v1")
public class UtilsControllerV1 {

    private final ExporterService exporterService;
    private final ModelService modelService;

    @Autowired
    public UtilsControllerV1(ExporterService exporterService,
                             ModelService modelService) {
        this.exporterService = exporterService;
        this.modelService = modelService;
    }

    /**
     * We use Callable interface here, because we want this method to timeout, when reach 30 seconds.
     */
    @ApiOperation(value = "Get list of all models pageable.")
    @PostMapping("/models")
    public Callable<PaginationResponse> getAllModels(@Valid @RequestBody PaginationRequest paginationRequest) {
        return () -> modelService.getAllModels(paginationRequest);
    }

    @ApiOperation(value = "Export word document by given type and id of object.")
    @GetMapping("/export/{blobId}")
    public void exportWord(HttpServletResponse response,
                           @RequestParam DocumentType type,
                           @PathVariable long blobId) throws IOException {
        ModelWithBLOB model = modelService.getModelWithBLOBById(blobId);
        exporterService.writeHeaderValue(response, type, model);
        exporterService.export(response, type, model);
    }
}