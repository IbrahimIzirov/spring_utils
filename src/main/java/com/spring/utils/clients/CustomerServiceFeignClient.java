package com.spring.utils.clients;

import com.spring.utils.config.ClientConfiguration;
import com.spring.utils.models.dto.ObjectBlobDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "customer-service-client", url = "${rest.url.customer-info}", configuration = ClientConfiguration.class)
public interface CustomerServiceFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "{id}/CustomerService", consumes = MediaType.APPLICATION_JSON_VALUE)
    ObjectBlobDto getCustomerById(@PathVariable long id);

    @RequestMapping(method = RequestMethod.POST, value = "create", consumes = MediaType.APPLICATION_JSON_VALUE)
    String createCustomer(@RequestBody String requestBody, @RequestParam String url, @RequestParam String info);

    @RequestMapping(method = RequestMethod.PUT, value = "update", consumes = MediaType.APPLICATION_JSON_VALUE)
    String updateCustomer(@RequestBody String requestBody, @RequestParam String url, @RequestParam String info);
}