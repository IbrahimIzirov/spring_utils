package com.spring.utils.constants;

import java.io.File;

public class UtilsConstants {

    //Exporter constants
    public static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
    public static final String HEADER_COOKIE = "Cookie";
    public static final String HEADER_COOKIE_VALUE = "django_language=bg";
    public static final String CONTENT_DISPOSITION_HEADER_KEY = "Content-Disposition";
    public static final String PATH_TO_CALISTOGA_REGULAR_FONT = "src/main/resources/fonts/Calistoga-Regular.ttf";

    //Path to templates
    public static final File DOCUMENT_WITH_IMAGE = new File("src/main/resources/templates/documentWithImage.docx");
    public static final String ABSOLUTE_PATH_DOCUMENT_WITH_IMAGE = DOCUMENT_WITH_IMAGE.getAbsolutePath();
    public static final File DOCUMENT_WITH_TABLE = new File("src/main/resources/templates/documentWithTable.docx");
    public static final String ABSOLUTE_PATH_DOCUMENT_WITH_TABLE = DOCUMENT_WITH_TABLE.getAbsolutePath();
    public static final File DOCUMENT_WITHOUT_TABLE = new File("src/main/resources/templates/documentWithoutTable.docx");
    public static final String ABSOLUTE_PATH_DOCUMENT_WITHOUT_TABLE = DOCUMENT_WITHOUT_TABLE.getAbsolutePath();

    //Standard
    public static final String STRING_WITH_CHECKBOX = "☑ %s";
    public static final String STRING_WITHOUT_CHECKBOX = "☐ %s";

    //Exceptions
    public static final String INVALID_INPUT_FOR_THE_FIELD = "Invalid input for the field '%s'.";
    public static final String ENTITY_WAS_NOT_FOUND = "Model was not found by given id: %s";
    public static final String EXCEPTION_OCCURRED = "An exception occurred: %s";
    public static final String PATTERN_FOR_METHOD_ARGUMENT_EXCEPTION = "[%s -> %s]";
    public static final String FONT_WAS_NOT_FOUND_EXCEPTION = "Given font was now found!";

    //Indexes hints for database
    public static final String INDEXES_NAMES_PROD = "/*+INDEX_DESC(p, ID_INDEX_NAME_PROD, EIK_INDEX_NAME_PROD, STATUS_INDEX,NAME_PROD)*/";
    public static final String INDEXES_NAMES_TEST = "/*+INDEX_DESC(p, ID_INDEX_NAME_TEST, EIK_INDEX_NAME_TEST, STATUS_INDEX,NAME_TEST)*/";

    //Statuses count for counting operation in query
    public static final String STATUSES_FOR_COUNTING_OPERATION = "and p.STATUS IN ('STATUS_ONE', 'STATUS_TWO', 'STATUS_THREE', " +
            "'STATUS_FOUR', 'STATUS_FIVE', 'STATUS_SIX')";
}