package com.spring.utils.constants;

import com.spring.utils.exceptions.InvalidInputException;

import java.util.Arrays;

import static com.spring.utils.constants.UtilsConstants.INVALID_INPUT_FOR_THE_FIELD;

public enum Profession {
    KIDS("Деца"),
    PENSIONERS("Пенсионери"),
    STUDENTS("Студенти"),
    EXPERTS("Експерти");

    private final String displayName;

    Profession(String displayName) {
        this.displayName = displayName;
    }

    public String displayName() {
        return displayName;
    }

    public static Profession fetchValue(String constant) {
        return Arrays.stream(Profession.values())
                .filter(e -> e.displayName.equalsIgnoreCase(constant))
                .findFirst()
                .orElseThrow(() -> new InvalidInputException(String.format(INVALID_INPUT_FOR_THE_FIELD, Profession.class.getSimpleName())));
    }
}