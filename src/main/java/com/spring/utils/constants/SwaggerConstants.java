package com.spring.utils.constants;

public class SwaggerConstants {
    public static final String RESPONSE_STATUS_OK = "Ok";
    public static final String RESPONSE_STATUS_BAD_REQUEST = "Bad Request";
    public static final String RESPONSE_STATUS_FORBIDDEN = "Forbidden";
    public static final String RESPONSE_STATUS_UNAUTHORIZED = "Unauthorized";
    public static final String RESPONSE_STATUS_NOT_FOUND = "Not found";
    public static final String RESPONSE_STATUS_CONFLICT = "Conflict";
    public static final String RESPONSE_STATUS_INTERNAL_SERVER_ERROR = "Internal server error";
}