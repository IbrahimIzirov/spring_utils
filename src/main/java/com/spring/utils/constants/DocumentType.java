package com.spring.utils.constants;

public enum DocumentType {
    DOCUMENT_WITH_IMAGE("Document_with_image"),
    DOCUMENT_WITH_TABLE("Document_with_table"),
    DOCUMENT_WITHOUT_TABLE("Document_without_table");

    private final String displayName;

    DocumentType(String displayName) {
        this.displayName = displayName;
    }

    public String displayName() {
        return displayName;
    }
}