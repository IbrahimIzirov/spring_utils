package com.spring.utils.annotations;

import com.spring.utils.annotations.validators.FiedlsConnotBeNullValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Annotation which check checkbox 2 and 3 for null based on checkbox 1.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = FiedlsConnotBeNullValidator.class)
public @interface FiedlsConnotBeNull {
    String message() default "Checkbox 2 and 3 can't be null!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}