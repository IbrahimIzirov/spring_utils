package com.spring.utils.annotations.validators;

import com.spring.utils.annotations.AtLeastOneMustBeSelected;
import com.spring.utils.exceptions.EntityNotFoundException;
import com.spring.utils.models.dto.ObjectClobDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AtLeastOneMustBeSelectedValidator implements ConstraintValidator<AtLeastOneMustBeSelected, ObjectClobDto> {
    @Override
    public void initialize(AtLeastOneMustBeSelected constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(ObjectClobDto value, ConstraintValidatorContext context) {
        List<String> values = new ArrayList<>();
        values.add(value.getCheckboxOne());
        values.add(value.getCheckboxTwo());
        values.add(value.getCheckboxThree());
        values.add(value.getCheckboxFour());
        values.add(value.getCheckboxFive());
        checkListIfAllValuesIsNull(values);
        return true;
    }

    private void checkListIfAllValuesIsNull(List<String> list) {
        List<String> nullValues = list.stream()
                .filter(Objects::isNull)
                .collect(Collectors.toList());

        if (nullValues.size() == list.size()) {
            throw new EntityNotFoundException("No one is selected");
        }
    }
}