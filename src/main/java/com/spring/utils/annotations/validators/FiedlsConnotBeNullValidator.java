package com.spring.utils.annotations.validators;

import com.spring.utils.annotations.FiedlsConnotBeNull;
import com.spring.utils.models.dto.ObjectBlobDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FiedlsConnotBeNullValidator implements ConstraintValidator<FiedlsConnotBeNull, ObjectBlobDto> {

    @Override
    public void initialize(FiedlsConnotBeNull constraintAnnotation) {
    }

    @Override
    public boolean isValid(ObjectBlobDto value, ConstraintValidatorContext context) {
        if (value.getCheckboxOne() != null) {
            return value.getCheckboxTwo() != null && value.getCheckboxThree() != null;
        }
        return true;
    }
}