package com.spring.utils.annotations;

import com.spring.utils.annotations.validators.AtLeastOneMustBeSelectedValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Annotation which check all fields in some object and one of them must be not null.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = AtLeastOneMustBeSelectedValidator.class)
public @interface AtLeastOneMustBeSelected {
    String message() default "At least one of all checkboxes must be selected!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}