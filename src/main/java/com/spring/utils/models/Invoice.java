package com.spring.utils.models;

import java.util.List;
import java.util.UUID;

public class Invoice {

    private String Id;
    private List<Item> items;

    public Invoice(List<Item> items) {
        Id = UUID.randomUUID().toString();
        this.items = items;
    }

    public Invoice() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "Id='" + Id + '\'' +
                ", items=" + items +
                '}';
    }
}