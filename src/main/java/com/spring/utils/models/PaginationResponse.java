package com.spring.utils.models;

import java.util.List;

/**
 * Pagination response object is used like return type of paginated operation
 *
 * @author Ibrahim Izirov
 * @since 05-04-2022
 */
public class PaginationResponse {

    private long totalRecords;
    private List<ModelWIthBLOBPaginated> models;

    public PaginationResponse() {
    }

    public long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<ModelWIthBLOBPaginated> getModels() {
        return models;
    }

    public void setModels(List<ModelWIthBLOBPaginated> models) {
        this.models = models;
    }
}