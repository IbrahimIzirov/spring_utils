package com.spring.utils.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * We make this class entity, but it's not required to give him name of table.
 * Return from query exactly count of each product
 *
 * @author Ibrahim Izirov
 * @since 10-05-2023
 */
@Entity
public class ReportProducts {

    @Id
    @Column(name = "STATUS_NAME")
    private String status;

    @Column(name = "LG_COUNTS")
    private Long lgCountNumber;

    @Column(name = "SONY_COUNTS")
    private Long sonyCountNumber;

    @Column(name = "NOKIA_COUNTS")
    private Long nokiaCountNumber;

    @Column(name = "SAMSUNG_COUNTS")
    private Long samsungCountNumber;

    @Column(name = "TOSHIBA_COUNTS")
    private Long toshibaCountNumber;

    @Column(name = "DELL_COUNTS")
    private Long dellCountNumber;

    public ReportProducts() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getLgCountNumber() {
        return lgCountNumber;
    }

    public void setLgCountNumber(Long lgCountNumber) {
        this.lgCountNumber = lgCountNumber;
    }

    public Long getSonyCountNumber() {
        return sonyCountNumber;
    }

    public void setSonyCountNumber(Long sonyCountNumber) {
        this.sonyCountNumber = sonyCountNumber;
    }

    public Long getNokiaCountNumber() {
        return nokiaCountNumber;
    }

    public void setNokiaCountNumber(Long nokiaCountNumber) {
        this.nokiaCountNumber = nokiaCountNumber;
    }

    public Long getSamsungCountNumber() {
        return samsungCountNumber;
    }

    public void setSamsungCountNumber(Long samsungCountNumber) {
        this.samsungCountNumber = samsungCountNumber;
    }

    public Long getToshibaCountNumber() {
        return toshibaCountNumber;
    }

    public void setToshibaCountNumber(Long toshibaCountNumber) {
        this.toshibaCountNumber = toshibaCountNumber;
    }

    public Long getDellCountNumber() {
        return dellCountNumber;
    }

    public void setDellCountNumber(Long dellCountNumber) {
        this.dellCountNumber = dellCountNumber;
    }
}