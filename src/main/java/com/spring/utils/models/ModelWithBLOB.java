package com.spring.utils.models;

import com.spring.utils.models.dto.ObjectBlobDto;
import com.vladmihalcea.hibernate.type.json.JsonBlobType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

/**
 * Object with one column - Blob type in oracle.
 *
 * @author Ibrahim Izirov
 * @since 25-01-2022
 */
@TypeDef(name = "jsonb", typeClass = JsonBlobType.class)
@Entity
@Table(name = "TABLE_WITH_BLOB", schema = "C##IZIROWW")
public class ModelWithBLOB {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "object_id")
    private long blobObjectId;

    @Column(name = "category_1")
    private String categoryOne;

    @Column(name = "category_2")
    private String categoryTwo;

    @Column(name = "category_3")
    private String categoryThree;

    @Column(name = "category_4")
    private String categoryFour;

    @Column(name = "profession")
    private String profession;

    @Type(type = "jsonb")
    @Column(name = "properties")
    private ObjectBlobDto properties;

    public ModelWithBLOB() {
    }

    public long getBlobObjectId() {
        return blobObjectId;
    }

    public void setBlobObjectId(long blobObjectId) {
        this.blobObjectId = blobObjectId;
    }

    public String getCategoryOne() {
        return categoryOne;
    }

    public void setCategoryOne(String categoryOne) {
        this.categoryOne = categoryOne;
    }

    public String getCategoryTwo() {
        return categoryTwo;
    }

    public void setCategoryTwo(String categoryTwo) {
        this.categoryTwo = categoryTwo;
    }

    public String getCategoryThree() {
        return categoryThree;
    }

    public void setCategoryThree(String categoryThree) {
        this.categoryThree = categoryThree;
    }

    public String getCategoryFour() {
        return categoryFour;
    }

    public void setCategoryFour(String categoryFour) {
        this.categoryFour = categoryFour;
    }

    public ObjectBlobDto getProperties() {
        return properties;
    }

    public void setProperties(ObjectBlobDto properties) {
        this.properties = properties;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}