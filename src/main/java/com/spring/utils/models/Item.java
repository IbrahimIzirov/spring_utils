package com.spring.utils.models;

public class Item {

    private String Barcode;
    private int quantity;
    private double price;

    public Item() {
    }

    public Item(String barcode, int quantity, double price) {
        Barcode = barcode;
        this.quantity = quantity;
        this.price = price;
    }

    public String getBarcode() {
        return Barcode;
    }

    public void setBarcode(String barcode) {
        Barcode = barcode;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "Barcode='" + Barcode + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}