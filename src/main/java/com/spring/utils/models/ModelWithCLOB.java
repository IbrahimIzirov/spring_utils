package com.spring.utils.models;

import com.spring.utils.models.dto.ObjectClobDto;
import com.vladmihalcea.hibernate.type.json.JsonType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

/**
 * Object with one column - Clob type in oracle.
 *
 * @author Ibrahim Izirov
 * @since 25-01-2022
 */
@TypeDef(name = "clob", typeClass = JsonType.class)
@Entity
@Table(name = "TABLE_WITH_CLOB", schema = "C##IZIROWW")
public class ModelWithCLOB {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "object_id")
    private long clobObjectId;

    @Column(name = "properties_1")
    private String propertiesOne;

    @Column(name = "properties_2")
    private String propertiesTwo;

    @Column(name = "properties_3")
    private String propertiesThree;

    @Column(name = "properties_4")
    private String propertiesFour;

    @Type(type = "clob")
    @Column(name = "documents")
    private ObjectClobDto documents;

    public ModelWithCLOB() {
    }

    public long getClobObjectId() {
        return clobObjectId;
    }

    public void setClobObjectId(long clobObjectId) {
        this.clobObjectId = clobObjectId;
    }

    public String getPropertiesOne() {
        return propertiesOne;
    }

    public void setPropertiesOne(String propertiesOne) {
        this.propertiesOne = propertiesOne;
    }

    public String getPropertiesTwo() {
        return propertiesTwo;
    }

    public void setPropertiesTwo(String propertiesTwo) {
        this.propertiesTwo = propertiesTwo;
    }

    public String getPropertiesThree() {
        return propertiesThree;
    }

    public void setPropertiesThree(String propertiesThree) {
        this.propertiesThree = propertiesThree;
    }

    public String getPropertiesFour() {
        return propertiesFour;
    }

    public void setPropertiesFour(String propertiesFour) {
        this.propertiesFour = propertiesFour;
    }

    public ObjectClobDto getDocuments() {
        return documents;
    }

    public void setDocuments(ObjectClobDto documents) {
        this.documents = documents;
    }
}