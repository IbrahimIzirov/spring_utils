package com.spring.utils.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Object with structure for pagination request
 *
 * @author Ibrahim Izirov
 * @since 05-04-2022
 */
public class PaginationRequest {

    @NotNull
    private Integer desiredPage;

    @NotNull
    private Integer maxRows;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss.SS")
    private LocalDateTime startDate;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss.SS")
    private LocalDateTime endDate;

    private String userInitials;

    private OrderData orderData;

    private FilterData filterData;

    public PaginationRequest() {
    }

    public Integer getDesiredPage() {
        return desiredPage;
    }

    public void setDesiredPage(Integer desiredPage) {
        this.desiredPage = desiredPage;
    }

    public Integer getMaxRows() {
        return maxRows;
    }

    public void setMaxRows(Integer maxRows) {
        this.maxRows = maxRows;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getUserInitials() {
        return userInitials;
    }

    public void setUserInitials(String userInitials) {
        this.userInitials = userInitials;
    }

    public OrderData getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderData orderData) {
        this.orderData = orderData;
    }

    public FilterData getFilterData() {
        return filterData;
    }

    public void setFilterData(FilterData filterData) {
        this.filterData = filterData;
    }
}