package com.spring.utils.models.dto;

import java.util.List;

/**
 * Object for filtering
 *
 * @author Ibrahim Izirov
 * @since 05-04-2022
 */
public class FilterData {

    private String id;
    private String firstName;
    private String lastName;
    private String category;
    private List<String> statuses;

    public FilterData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<String> statuses) {
        this.statuses = statuses;
    }
}