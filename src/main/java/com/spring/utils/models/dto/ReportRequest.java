package com.spring.utils.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class ReportRequest {

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss.SS")
    private LocalDateTime startDate;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss.SS")
    private LocalDateTime endDate;

    private String userInitials;
    private String status;
    private String product;

    public ReportRequest() {
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getUserInitials() {
        return userInitials;
    }

    public void setUserInitials(String userInitials) {
        this.userInitials = userInitials;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}