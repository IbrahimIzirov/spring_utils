package com.spring.utils.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object example for blob
 *
 * @author Ibrahim Izirov
 * @since 25-01-2022
 */
public class ObjectBlobDto {
    private String checkboxOne;
    private String checkboxTwo;
    private String checkboxThree;
    private String checkboxFour;
    private String checkboxFive;
    private String documentThree;

    // It is needed else Jackson will look got getValid method and will fail.
    //With this property, we can change name of getter --> 'isValid'
    @JsonProperty("valid")
    private Boolean valid;

    public ObjectBlobDto() {
    }

    public String getCheckboxOne() {
        return checkboxOne;
    }

    public void setCheckboxOne(String checkboxOne) {
        this.checkboxOne = checkboxOne;
    }

    public String getCheckboxTwo() {
        return checkboxTwo;
    }

    public void setCheckboxTwo(String checkboxTwo) {
        this.checkboxTwo = checkboxTwo;
    }

    public String getCheckboxThree() {
        return checkboxThree;
    }

    public void setCheckboxThree(String checkboxThree) {
        this.checkboxThree = checkboxThree;
    }

    public String getCheckboxFour() {
        return checkboxFour;
    }

    public void setCheckboxFour(String checkboxFour) {
        this.checkboxFour = checkboxFour;
    }

    public String getCheckboxFive() {
        return checkboxFive;
    }

    public void setCheckboxFive(String checkboxFive) {
        this.checkboxFive = checkboxFive;
    }

    public String getDocumentThree() {
        return documentThree;
    }

    public void setDocumentThree(String documentThree) {
        this.documentThree = documentThree;
    }

    public Boolean isValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}