package com.spring.utils.models.dto;

/**
 * Object for ordering
 *
 * @author Ibrahim Izirov
 * @since 05-04-2022
 */
public class OrderData {

    private String id;
    private String firstName;
    private String lastName;
    private String category;

    public OrderData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}