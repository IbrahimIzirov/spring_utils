package com.spring.utils.models;

import com.spring.utils.models.dto.ObjectBlobDto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TABLE_NAME", schema = "SCHEMA_NAME")
public class Application {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "TYPE_ID")
    private Long typeId;

    @Column(name = "INSTANCE")
    private String instance;

    @Column(name = "OFFER_TYPE")
    private String offerType;

    @Column(name = "EIK")
    private String eik;

    @Column(name = "NAME_LATIN")
    private String nameLatin;

    @Column(name = "NAME")
    private String fullName;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PRODUCT")
    private String product;

    @Column(name = "AMOUNT")
    private Long amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BLOB_COLUMN")
    private ObjectBlobDto blobDto;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "DATE_LOADED")
    private Date dateLoaded;

    @Column(name = "LINE_CODE")
    private String lineCode;

    public Application() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getEik() {
        return eik;
    }

    public void setEik(String eik) {
        this.eik = eik;
    }

    public String getNameLatin() {
        return nameLatin;
    }

    public void setNameLatin(String nameLatin) {
        this.nameLatin = nameLatin;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public ObjectBlobDto getBlobDto() {
        return blobDto;
    }

    public void setBlobDto(ObjectBlobDto blobDto) {
        this.blobDto = blobDto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateLoaded() {
        return dateLoaded;
    }

    public void setDateLoaded(Date dateLoaded) {
        this.dateLoaded = dateLoaded;
    }

    public String getLineCode() {
        return lineCode;
    }

    public void setLineCode(String lineCode) {
        this.lineCode = lineCode;
    }
}