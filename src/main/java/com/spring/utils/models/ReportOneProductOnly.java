package com.spring.utils.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * We make this class entity, but it's not required to give him name of table.
 * Return from query exactly count of one product only.
 *
 * @author Ibrahim Izirov
 * @since 10-05-2023
 */
@Entity
public class ReportOneProductOnly {

    @Id
    @Column(name = "STATUS_NAME")
    private String status;

    @Column(name = "COUNTS")
    private Long countNumber;

    public ReportOneProductOnly() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCountNumber() {
        return countNumber;
    }

    public void setCountNumber(Long countNumber) {
        this.countNumber = countNumber;
    }
}