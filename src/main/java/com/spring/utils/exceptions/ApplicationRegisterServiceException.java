package com.spring.utils.exceptions;

/**
 * Exception is thrown from rest template when we call service
 *
 * @author Ibrahim Izirov
 * @since 31-08-2022
 */
public class ApplicationRegisterServiceException extends RuntimeException {

    public ApplicationRegisterServiceException(String errorMessage) {
        super(errorMessage);
    }
}