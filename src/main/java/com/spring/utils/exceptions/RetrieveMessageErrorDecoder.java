package com.spring.utils.exceptions;

import feign.Response;
import feign.codec.ErrorDecoder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 * Class for maintainence errors from feign client
 */
public class RetrieveMessageErrorDecoder implements ErrorDecoder {

    private final ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {
        String errorMessage = null;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.body().asInputStream()))) {
            errorMessage = reader.lines().collect(Collectors.joining(""));
        } catch (IOException e) {
            return new Exception(e.getMessage());
        }
        switch (response.status()) {
            case 400:
            case 404:
                throw new InvalidInputException(errorMessage);
            default:
                return errorDecoder.decode(methodKey, response);
        }
    }
}
