package com.spring.utils.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.stream.Collectors;

import static com.spring.utils.constants.UtilsConstants.EXCEPTION_OCCURRED;
import static com.spring.utils.constants.UtilsConstants.PATTERN_FOR_METHOD_ARGUMENT_EXCEPTION;

/**
 * Global exception handler that handles exceptions thrown from services.
 *
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * Method who export ModelWithBLOB in word file.
     *
     * @param exception - caught exception
     * @return Entity with message and status
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        LOGGER.warn(String.format(EXCEPTION_OCCURRED, exception
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .map(fieldError -> String.format(PATTERN_FOR_METHOD_ARGUMENT_EXCEPTION,
                        fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.joining(", "))));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(String.format(EXCEPTION_OCCURRED, exception
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .map(fieldError -> String.format(PATTERN_FOR_METHOD_ARGUMENT_EXCEPTION,
                        fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.joining(", "))));
    }

    /**
     * Method that handles EntityNotFoundException
     *
     * @param exception - caught EntityNotFoundException
     * @return Entity with message and status
     */
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity entityNotFoundException(EntityNotFoundException exception) {
        LOGGER.warn(String.format(EXCEPTION_OCCURRED, exception.getMessage()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
    }

    /**
     * Method that handles InvalidInputException
     *
     * @param exception - caught InvalidInputException
     * @return Entity with message and status
     */
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = InvalidInputException.class)
    public ResponseEntity invalidInputException(InvalidInputException exception) {
        LOGGER.warn(String.format(EXCEPTION_OCCURRED, exception.getMessage()));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }
}