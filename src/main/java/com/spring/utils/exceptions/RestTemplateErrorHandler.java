package com.spring.utils.exceptions;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;

/**
 * RestTemplate error handler class
 *
 * @author Ibrahim Izirov
 * @since 31-08-2021
 */
public class RestTemplateErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        return (httpResponse.getStatusCode().series() == CLIENT_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        if (response.getStatusCode().is4xxClientError()) {

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getBody()))) {
                String errorMessage = reader.lines().collect(Collectors.joining(""));
                throw new ApplicationRegisterServiceException(errorMessage);
            }
        }
    }
}