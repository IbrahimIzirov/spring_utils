package com.spring.utils.exceptions;

/**
 * Exception thrown from the service when Invalid input type is present
 *
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
public class InvalidInputException extends RuntimeException {

    public InvalidInputException(String message) {
        super(message);
    }
}