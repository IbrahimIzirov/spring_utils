package com.spring.utils.exceptions;

/**
 * Exception thrown form the service when Entity was not found.
 *
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super(message);
    }
}