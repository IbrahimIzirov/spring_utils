package com.spring.utils.constants;

import com.spring.utils.models.dto.FilterData;
import com.spring.utils.models.dto.OrderData;
import com.spring.utils.models.dto.PaginationRequest;

import java.time.LocalDateTime;

/**
 * Class for create mock objects.
 *
 * @author Ibrahim Izirov
 * @since 28-07-2022
 */
public class ModelBuilder {

    public static PaginationRequest mockPaginationRequest() {
        PaginationRequest paginationRequest = new PaginationRequest();
        paginationRequest.setDesiredPage(1);
        paginationRequest.setMaxRows(10);
        paginationRequest.setStartDate(LocalDateTime.of(2022, 2, 5, 17, 10));
        paginationRequest.setEndDate(LocalDateTime.of(2022, 4, 5, 17, 10));
        paginationRequest.setUserInitials("ibra");
        paginationRequest.setFilterData(mockFilterData());
        paginationRequest.setOrderData(mockOrderData());

        return paginationRequest;
    }

    private static OrderData mockOrderData() {
        OrderData orderData = new OrderData();
        orderData.setId(null);
        orderData.setFirstName(null);
        orderData.setLastName(null);
        orderData.setCategory(null);
        return orderData;
    }

    private static FilterData mockFilterData() {
        FilterData filterData = new FilterData();
        filterData.setId(null);
        filterData.setFirstName("Ibrahim");
        filterData.setLastName(null);
        filterData.setCategory(null);
        return filterData;
    }
}
