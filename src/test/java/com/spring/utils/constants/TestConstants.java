package com.spring.utils.constants;

public class TestConstants {
    public static final String EXAMPLE = "Message for exception";
    public static final String HEADER_KEY_CONTENT = "Content-Disposition";
    public static final String HEADER_VALUE_CONTENT = "attachment; filename=Document_with_image_EXAMPLE_NAME.docx";
}