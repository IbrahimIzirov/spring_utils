package com.spring.utils.services;

import com.spring.utils.exceptions.EntityNotFoundException;
import com.spring.utils.models.ModelWIthBLOBPaginated;
import com.spring.utils.models.ModelWithBLOB;
import com.spring.utils.models.dto.PaginationRequest;
import com.spring.utils.repositories.ModelRepository;
import com.spring.utils.repositories.PaginatedWithJPA;
import com.spring.utils.services.impl.ModelServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.spring.utils.constants.ModelBuilder.mockPaginationRequest;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Ibrahim Izirov
 * @since 12-06-2023
 */
@ExtendWith(MockitoExtension.class)
public class ModelServiceTests {

    @Mock
    private ModelRepository modelRepository;

    @Mock
    private PaginatedWithJPA jpaRepository;

    @InjectMocks
    private ModelServiceImpl modelService;

    ModelWithBLOB blob;
    List<ModelWIthBLOBPaginated> listPaginated;
    PaginationRequest paginationRequest;

    @BeforeEach
    public void beforeEach() {
        blob = mock(ModelWithBLOB.class);
        listPaginated = new ArrayList<>();
        paginationRequest = mockPaginationRequest();
    }

    @Test
    public void getBlobById_Should_Thrown_When_DoesNot_Exist() {
        assertThrows(EntityNotFoundException.class,
                () -> modelService.getModelWithBLOBById(anyInt()));
    }

    @Test
    public void getAll_Should_Return_Response_From_Repository() {
        when(jpaRepository.findAllModels(anyString(), anyString(), anyString(), anyString(), anyString(),
                anyString(), anyString(), anyString(), anyString(), anyString(), anyInt(), anyInt()))
                .thenReturn(listPaginated);

        modelService.getAllModels(paginationRequest);

        verify(jpaRepository, times(1))
                .findAllModels(anyString(), anyString(), anyString(), anyString(), anyString(),
                        anyString(), anyString(), anyString(), anyString(), anyString(), anyInt(), anyInt());
    }
}