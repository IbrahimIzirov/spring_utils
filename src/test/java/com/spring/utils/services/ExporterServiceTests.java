package com.spring.utils.services;

import com.spring.utils.constants.DocumentType;
import com.spring.utils.models.ModelWithBLOB;
import com.spring.utils.services.impl.ExporterServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.spring.utils.constants.TestConstants.HEADER_KEY_CONTENT;
import static com.spring.utils.constants.TestConstants.HEADER_VALUE_CONTENT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Ibrahim Izirov
 * @since 12-06-2023
 */
@ExtendWith(MockitoExtension.class)
public class ExporterServiceTests {

    @Mock
    ExporterService exService;

    @InjectMocks
    ExporterServiceImpl exServiceImpl;

    ModelWithBLOB blob;
    HttpServletResponse response;
    ServletOutputStream outputStream;

    @BeforeEach
    public void beforeEach() {
        blob = mock(ModelWithBLOB.class);
        response = mock(HttpServletResponse.class);
        outputStream = mock(ServletOutputStream.class);
    }

    @Test
    public void export_Should_Execute_ExporterService_With_Image() throws IOException {
        when(response.getOutputStream()).thenReturn(outputStream);

        exServiceImpl.export(response, DocumentType.DOCUMENT_WITH_IMAGE, blob);
    }

    @Test
    public void export_Should_Execute_ExporterService_With_Table() throws IOException {
        when(response.getOutputStream()).thenReturn(outputStream);

        exServiceImpl.export(response, DocumentType.DOCUMENT_WITH_TABLE, blob);
    }

    @Test
    public void write_HeaderValue_InResponse() {
        when(response.getHeader(HEADER_KEY_CONTENT))
                .thenReturn(HEADER_VALUE_CONTENT);

        exServiceImpl.writeHeaderValue(response, DocumentType.DOCUMENT_WITH_IMAGE, blob);

        assertEquals(response.getHeader(HEADER_KEY_CONTENT), HEADER_VALUE_CONTENT);
    }
}