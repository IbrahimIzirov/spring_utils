package com.spring.utils.annotations;

import com.spring.utils.annotations.validators.AtLeastOneMustBeSelectedValidator;
import com.spring.utils.models.dto.ObjectClobDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests validations for AtLeastOneMustBeSelected
 */
public class AtLeastOneMustBeSelectedTests {

    @InjectMocks
    AtLeastOneMustBeSelectedValidator validator;

    @Mock
    ConstraintValidatorContext context;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isValid_test() {
        ObjectClobDto clob = new ObjectClobDto();
        clob.setCheckboxFour("TEST");
        boolean result = validator.isValid(clob, context);
        assertTrue(result);
    }
}