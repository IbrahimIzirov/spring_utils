package com.spring.utils.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.utils.models.dto.PaginationRequest;
import com.spring.utils.repositories.ModelRepository;
import com.spring.utils.repositories.PaginatedWithJPA;
import com.spring.utils.services.ExporterService;
import com.spring.utils.services.ModelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import static com.spring.utils.constants.ModelBuilder.mockPaginationRequest;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Testing Utils Controller V1 endpoints
 *
 * @author Ibrahim Izirov
 * @since 28-07-2022
 */
@ActiveProfiles("test")
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = UtilsControllerV1.class)
public class UtilsControllerV1Tests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExporterService exporterService;

    @MockBean
    private ModelService modelService;

    @MockBean
    private ModelRepository modelRepository;

    @MockBean
    private PaginatedWithJPA paginatedWithJPA;

    @MockBean
    private JwtDecoder jwtDecoder;

    @InjectMocks
    UtilsControllerV1 utilsControllerV1;

    PaginationRequest mockPaginationRequest;
    ObjectMapper mapper;

    @BeforeEach
    public void setup() {
        mockPaginationRequest = mockPaginationRequest();
        mapper = new ObjectMapper();
    }

    @Test
    void getAllModels_isOk() throws Exception {
        mapper.findAndRegisterModules();

        RequestBuilder requestBuilder = post(
                "/v1/models")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(mockPaginationRequest));

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }
}