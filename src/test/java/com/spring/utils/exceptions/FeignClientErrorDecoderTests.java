package com.spring.utils.exceptions;

import feign.Request;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Collections;

import static feign.Util.UTF_8;
import static org.codehaus.groovy.tools.Utilities.repeatString;

/**
 * @author Ibrahim Izirov
 * @since 12-06-2023
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class FeignClientErrorDecoderTests {

    @Mock
    private ErrorDecoder errorDecoder;

    @InjectMocks
    private RetrieveMessageErrorDecoder decoder;

    @ParameterizedTest
    @ValueSource(ints = {400, 404, 500})
    public void throwsFeignExceptionBody(int statusValue) {
        String actualBody = repeatString("hello world", 2);
        Response response = Response.builder()
                .status(statusValue)
                .reason("Bad Request")
                .request(Request.create(Request.HttpMethod.GET, "/api", Collections.emptyMap(), null, UTF_8))
                .body(actualBody, UTF_8)
                .build();

        try {
            throw decoder.decode("Service#Customer()", response);
        } catch (Exception ignored) {}
    }
}