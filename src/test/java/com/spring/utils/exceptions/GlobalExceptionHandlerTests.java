package com.spring.utils.exceptions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import static com.spring.utils.constants.TestConstants.EXAMPLE;

/**
 * @author Ibrahim Izirov
 * @since 16-02-2022
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class GlobalExceptionHandlerTests {

    @InjectMocks
    private GlobalExceptionHandler globalExceptionHandler;

    /**
     * Testing entityNotFoundException functionality and asserting that a EntityNotFoundException was thrown.
     */
    @Test
    public void entityNotFoundExceptionHandler_Test() {
        EntityNotFoundException exception = new EntityNotFoundException(EXAMPLE);
        ResponseEntity result = globalExceptionHandler.entityNotFoundException(exception);
        Assert.notNull(result, EXAMPLE);
    }

    /**
     * Testing invalidInputException functionality and asserting that a InvalidInputException was thrown.
     */
    @Test
    public void invalidInputExceptionHandler_Test() {
        InvalidInputException exception = new InvalidInputException(EXAMPLE);
        ResponseEntity result = globalExceptionHandler.invalidInputException(exception);
        Assert.notNull(result, EXAMPLE);
    }
}