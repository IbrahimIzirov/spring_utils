package com.spring.utils.exceptions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;

/**
 * This class tested the RestTemplateErrorHandler, where errors was catched from restTemplate
 *
 * @author Ibrahim Izirov
 * @since 31-08-2022
 */
@ExtendWith(MockitoExtension.class)
public class RestClientResponseExceptionTest {

    private static final String URL = "/v1/create-application";
    private MockRestServiceServer mockRestServiceServer;
    private RestTemplate restTemplate;

    @BeforeEach
    public void setUp() {
        restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new RestTemplateErrorHandler());
        mockRestServiceServer = MockRestServiceServer.createServer(restTemplate);
    }

    /**
     * Testing RestTemplateErrorHandler class and asserting that an ApplicationRegisterServiceException was thrown.
     */

    @Test
    public void Response_4xx() {
        mockRestServiceServer
                .expect(requestTo(URL))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withBadRequest());

        assertThrows(ApplicationRegisterServiceException.class, () -> {
            consumeWebService(URL, Object.class);
        });
    }

    /**
     * Testing RestTemplateErrorHandler class and asserting that when status code is 500 nothing happen.
     */
    @Test
    public void Response_5xx() {
        mockRestServiceServer
                .expect(requestTo(URL))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withServerError());

        ResponseEntity responseEntity = consumeWebService(URL, Object.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Method who consume mock web service
     */
    <T> ResponseEntity consumeWebService(String url, Class<T> responseType) {
        try {
            return restTemplate.getForEntity(url, responseType);
        } catch (RestClientResponseException e) {
            return ResponseEntity
                    .status(e.getRawStatusCode())
                    .body(e.getResponseBodyAsString());
        }
    }
}