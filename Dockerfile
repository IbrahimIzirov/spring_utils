FROM gradle:6.8.2-jdk11 AS BUILD
WORKDIR /app
COPY --chown=gradle:gradle . .
RUN gradle clean build --info
FROM openjdk:11
COPY --from=BUILD app/build/libs/spring-utils-svc-1.0.0.jar spring-utils-svc-1.0.0.jar
ENTRYPOINT ["java", "-jar", "/spring-utils-svc-1.0.0.jar"]
