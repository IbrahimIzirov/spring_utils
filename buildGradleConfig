//Spring version configuration
buildscript {
    ext {
        springBootVersion = "2.6.3"
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.6.3")
        classpath("io.spring.gradle:dependency-management-plugin:1.0.11.RELEASE")
    }
}

//Plugins
//apply from: cdaScript
apply plugin: "java"
apply plugin: "io.spring.dependency-management"
apply plugin: "jacoco"
apply plugin: "org.springframework.boot"

ext {
    nameOfSomeValue = "2.17.2"
}

group = 'spring-utils-svc'
version = '1.0.0'
sourceCompatibility = '11'
targetCompatibility = '11'

repositories {
    mavenCentral()
}

dependencies {
    compile "org.springframework.boot:spring-boot-starter-web"
    compile "org.springframework.boot:spring-boot-starter-actuator"
    compile "org.springframework.boot:spring-boot-starter-data-jpa"
    compile "org.springframework.boot:spring-boot-starter-security"
    compile "org.springframework.boot:spring-boot-starter-validation"
    compile "org.springframework.boot:spring-boot-devtools"
    compile "io.swagger:swagger-annotations:1.6.2"
    implementation group: 'io.springfox', name: 'springfox-swagger-ui', version: '2.9.2'
    implementation group: 'io.springfox', name: 'springfox-swagger2', version: '3.0.0'
    compile group: 'org.jacoco', name: 'org.jacoco.agent', version: '0.8.7'

    testImplementation('org.springframework.boot:spring-boot-starter-test') {
        exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
    }
}

compileJava {
    options.encoding = "UTF-8"
    options.annotationProcessorPath = configurations.annotationProcessor
}

bootJar {
    enabled = true
    archiveFileName = 'spring-utils-svc-1.0.0.jar'
}

dependencyManagement {
    imports {
        mavenBom "org.springframework.boot:spring-boot-starter-parent:${springBootVersion}"
    }
}

jacoco {
    toolVersion = "0.8.7"
    repoortDir = file("$buildDir/jacoco")
}

test {
    useJUnitPlatform()
    finalizedBy jacocoTestReport
}

jacocoTestReport {
    dependsOn test
    dependsOn test // test are required to run before generating the jacoco report
    reports {
        xml.enabled true
        //for XML
        xml.destination file("${buildDir}/reports/jacoco.xml)")
    }
}